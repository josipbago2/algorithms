/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstraalgorithm;

import java.util.ArrayList;

/**
 *
 * @author Josip
 * @param <T>
 */
public class TreeNode<T> {
    
    private T data = null;
    private TreeNode<T> parent = null;
    private ArrayList<TreeNode<T>> children = new ArrayList<>();
    
    public TreeNode(T data){
        this.data = data;
    }
    
    public TreeNode(T data, TreeNode<T> parent){
        this.data = data;
        this.parent = parent;
    }
    
    public ArrayList<TreeNode<T>> getChildren(){
        return children;
    }
    
    public void setParent(TreeNode<T> parent){
        parent.addChild(this);
        this.parent = parent;
    }
    
    public void addChild(T data){
        TreeNode<T> child = new TreeNode<>(data);
        child.setParent(this);
        this.children.add(child);
    }
    
    public void addChild(TreeNode<T> child){
        child.setParent(this);
        this.children.add(child);
    }
    
    public T getData(){
        return this.data;
    }
    
    public void setData(T data){
        this.data = data;
    }
    
    public boolean isRoot(){
        return (this.parent == null);
    }
    
    public boolean isLeaf(){
        return (this.children.isEmpty());
    }
    
    public void removeParent(){
        this.parent = null;
    }
}
