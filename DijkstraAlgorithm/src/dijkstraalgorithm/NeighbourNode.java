/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstraalgorithm;

/**
 *
 * @author Josip
 */
public class NeighbourNode {
    
    int nodeIndex;
    double edgeWeight;
    
    public NeighbourNode(int nodeIndex, double edgeWeight){
        this.nodeIndex = nodeIndex;
        this.edgeWeight = edgeWeight;
    }

    public int getNodeIndex() {
        return nodeIndex;
    }

    public double getEdgeWeight() {
        return edgeWeight;
    }
    
    
}
