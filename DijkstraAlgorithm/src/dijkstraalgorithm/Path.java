/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstraalgorithm;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Josip
 */
public class Path {
    
    private int pathWeight = 0;
    private Set<Integer> nodesInPath;

    public Path() {
        nodesInPath = new HashSet<>();
    }
    
    public void addNodeToPath(int nodeIndex, int edgeWeight){
        this.pathWeight += edgeWeight;
        nodesInPath.add(nodeIndex);
    }

    public int getPathWeight() {
        return pathWeight;
    }

    public Set<Integer> getNodesInPath() {
        return nodesInPath;
    }
    
}
