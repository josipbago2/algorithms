/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstraalgorithm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Josip
 */
public class DijkstraAlgorithm {
    
    private static final double INFINITY = Double.POSITIVE_INFINITY;
    private static int currentIndex, sourceNode = 0;
    
    private static Map<Integer, ArrayList<NeighbourNode>> networkData = new HashMap<>();
    private static Map<Integer, Double> pathDistances = new HashMap<>();
    
    private static Map<Integer, Path> pathsToNodes = new HashMap<>();

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            getNetworkBranches("");
        } catch (IOException | ArrayIndexOutOfBoundsException exception) {
            if(exception instanceof ArrayIndexOutOfBoundsException){
                System.out.println("You must input arguments: file path to graph data and index of a source node");
            }else{
                System.out.println("File path is not valid, or there was a mistake reading a file");
            }
        }
        
        // input source node
        currentIndex = sourceNode;
        // update path data for a source node to 0
        pathDistances.put(sourceNode, 0d);
        
        for(int i = 0; i < networkData.size(); i++){
            networkData.get(currentIndex).forEach((node) -> {
                pathDistances.put(node.getNodeIndex(),
                        findMinimumValue(pathDistances.get(node.getNodeIndex()), pathDistances.get(currentIndex) + node.getEdgeWeight()));
            });
        }
    }
    
    public static double findMinimumWeight(Map<Integer, Double> pathDistances){
        double minumumWeight = Collections.min(pathDistances.values());
        
        return minumumWeight;
    }
    
    public static double findMinimumValue(double currentPathWeight, double proposedWeight){
        return currentPathWeight > proposedWeight ? proposedWeight : currentPathWeight;
    }

    /**
     * Reads network data from a text file (example of line a:b.c, d.e) and
     * organizes network data (nodes, branches and their weights) into an
     * adjacency list of nodes with objects containing data about their
     * neighbour nodes and weights
     *
     * @param fileName uri file path to a text file with network data
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void getNetworkBranches(String fileName) throws IOException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
        String line;
        System.out.println("Fetching network data:");
        while ((line = br.readLine()) != null) {
            ArrayList<NeighbourNode> neighbourNodes = new ArrayList<>();
            int nodeIndex = Integer.parseInt(line.split(":")[0]);
            for (String i : line.split(":")[1].split(",")) {
               neighbourNodes.add(new NeighbourNode(
                        Integer.parseInt(i.trim().split(".")[0]),
                        Double.parseDouble(i.trim().split(".")[1])));
            }
            // storing network data as adjacency list
            networkData.put(nodeIndex, neighbourNodes);
            // initializing weights to infinity for all nodes
            pathDistances.put(nodeIndex, INFINITY);
            // initializing collection to store shortest paths data
            pathsToNodes.put(nodeIndex, new Path());
        }
    }

}
