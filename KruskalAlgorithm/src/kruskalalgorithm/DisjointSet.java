/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kruskalalgorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * A class with methods and classes which are used for algorithm execution
 * by using union and find methods with a path compression technique to search for parent nodes and group nodes in subsets.
 * @author Josip
 */
public class DisjointSet {   
    
    /**
     * Represents a class for creating subsets where nodes are grouped during algorithm execution.
     */
    public static class Subset {
        int parent, rank;

        /**
         * 
         * @return parent node of a subset
         */
        public int getParent() {
            return parent;
        }

        /**
         * 
         * @param parent for setting parent node of a subset
         */
        public void setParent(int parent) {
            this.parent = parent;
        }

        /**
         * 
         * @param rank for setting rank of a node in a subset
         */
        public void setRank(int rank) {
            this.rank = rank;
        }

        /**
         * 
         * @return rank of a parent node
         */
        public int getRank() {
            return rank;
        }
        
        /**
         * Constructor to initalize a subset
         * @param parent used for specifying parent node of a subset
         * @param rank used for specifying rank of a node in a subset
         */
        public Subset(int parent, int rank){
            this.parent = parent;
            this.rank = rank;
        }
    }
    
    /**
     * Method for finding a subset of a node, which is represented by an index of a parent node of that subset
     * @param subsets a list of subsets to be analyzed
     * @param i index of a node for which a subset needs to be found
     * @return parent node of a subset where a node is located
     */
    public static int find(List<Subset> subsets, int i){
        if(subsets.get(i).getParent() != i){
            subsets.get(i).setParent(find(subsets, subsets.get(i).getParent()));
        }
        return subsets.get(i).getParent();
    }
    
    /**
     * A method which does union of two subsets from both nodes
     * @param subsets a list of subsets
     * @param a index of a node representing one subset that needs to be unified
     * @param b index of a node representing one subset that needs to be unified
     */
    public static void union(List<Subset> subsets, int a, int b){
        int aroot = find(subsets, a);
        int broot = find(subsets, b);
        
        if (subsets.get(aroot).getRank() < subsets.get(broot).getRank()){
            subsets.get(aroot).setParent(broot);
        } else if (subsets.get(aroot).getRank() > subsets.get(broot).getRank()){
            subsets.get(broot).setParent(aroot);
        } else {
            subsets.get(broot).setParent(aroot);
            subsets.get(aroot).setRank(subsets.get(aroot).getRank() + 1);
        }
    }
}
