/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kruskalalgorithm;

/**
 * Represents a branch in a network.
 * @author Josip
 */

public class Branch {
    
    private int edgeWeight;
    private int nodeIndex1, nodeIndex2;

    /**
     * 
     * @return edge weight for this branch
     */
    public int getEdgeWeight() {
        return edgeWeight;
    }

    /**
     * 
     * @return index of source node
     */
    public int getNodeIndex1() {
        return nodeIndex1;
    }


    /**
     * 
     * @return index of destination node
     */
    public int getNodeIndex2() {
        return nodeIndex2;
    }

    /**
     * Constructor for Branch class where an object is instantiated representing a branch
     * @param node1 specify index of source node
     * @param node2 specify index of destination node
     * @param edgeWeight specify weight of edge for this branch
     */
    public Branch(int node1, int node2, int edgeWeight){
        this.edgeWeight = edgeWeight;
        this.nodeIndex1 = node1;
        this.nodeIndex2 = node2;
    }
    
}
