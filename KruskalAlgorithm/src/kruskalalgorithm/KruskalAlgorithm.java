/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kruskalalgorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Josip
 * 
 */
public class KruskalAlgorithm {
    
    // counters to track progress in log
    private static int counter = 1, counterMst = 1;
    private static int counterSubsets = 1;
    
    // datasets to store network data
    private static List<Branch> listBranches;
    private static Set<Integer> nodes = new HashSet<>();
    private static Set<Integer> edges = new HashSet<>();
    
    /**
     * @param minSpanninTree the command line arguments
     */
    private static List<Branch> minSpanninTree = new ArrayList<>();
    private static List<DisjointSet.Subset> subsets = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        listBranches = new ArrayList<>();
        try {
            listBranches.addAll(sortBranchesByWeights(getNetworkBranches("")));
        } catch(IOException e){
            e.printStackTrace();
        }

        System.out.print("Step " + counter++ 
                + "\nPick branches sorted by weight starting with the smallest and add them to "
                + "MST, skip those that form cycles\n\n");
        
        System.out.print("Initialize subsets, each subset contains one "
                + "node which is also a root node of that subset\n");
        
        for(Integer nodeIndex : nodes){
            subsets.add(new DisjointSet.Subset(nodeIndex, 0));
        }
        System.out.print("\n\n");
        
        for(Branch branch : listBranches){
            addBranchToMstIfNotCycle(branch);

        }
        
        System.out.print("\nMinimum spanning tree:\n\n");
        for(Branch branch : minSpanninTree){
            System.out.print("Branch:\n" + "Node 1: " 
                    + branch.getNodeIndex1() + "\nNode 2: " 
                    + branch.getNodeIndex2() + "\nEdge weight: " 
                    + branch.getEdgeWeight() + "\n\n");
        }
        
    }
    
    /**
     * Method for adding a branch to minimum spanning tree. It checks 
     * if adding a branch to existing minimum spanning
     * tree is going to create a cycle inside a tree. 
     * If there is a cycle in a spanning tree after adding a branch,
     * branch is not added. If there is no cycle after 
     * adding a branch then a branch is added to minimum spanning tree.
     * 
     * @param branch is a branch algorithm wants to add to a minimum spanning tree
     */
    public static void addBranchToMstIfNotCycle(Branch branch){
        int a = DisjointSet.find(subsets, branch.getNodeIndex1());
        int b = DisjointSet.find(subsets, branch.getNodeIndex2());
        System.out.print(counterMst++ + ". Adding branch\n");
        counterSubsets = 0;
        if(a != b){
            minSpanninTree.add(branch);
            DisjointSet.union(subsets, a, b);
            for(DisjointSet.Subset i : subsets){
                System.out.print("\tNode " + counterSubsets++ +": parent node " 
                        + i.getParent() + ", rank " + i.getRank() + "\n");
            }
            System.out.print("\tNo cycles, branch with weight " 
                    + branch.getEdgeWeight() + " added to MST.\n\n");
        }else{
            System.out.print("");
            System.out.print("\tSkipping branch, there is a cycle in MST when added.\n\n");
        }
    }
    
   
    /**
     * Method to sort list of branches by weight in ascending order
     * @param listOfBranches a list of branches to be sorted in ascending order by weight
     * @return returns a list of branches sorted by weight in ascending order
     */
    public static List<Branch> sortBranchesByWeights(List<Branch> listOfBranches){
        System.out.print("Step " + counter++ + "\nSorting branches by weight (ascending) ...");
        Collections.sort(listOfBranches, (Branch o1, Branch o2) -> o1.getEdgeWeight() - o2.getEdgeWeight());
        System.out.println("Branches sorted by weight from smallest to highest\n\n");
        return listOfBranches;
    }
    
    /**
     * Method to read a file containing network data.
     * @param fileName a path to a file containing network data
     * @return returns a list of branches from a network specified inside a file
     * @throws IOException
     * @throws FileNotFoundException 
     */
    public static List<Branch> getNetworkBranches(String fileName) throws IOException, FileNotFoundException {
        List<Branch> list = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(new File("C:\\Users\\Josip\\Desktop\\Algorithms\\algorithms\\graph.txt")));
	String line;
        System.out.println("Fetching network data:");
	while ((line = br.readLine()) != null) {
            String[] parameter = line.split(",");
		if (parameter.length == 4) {
                    Branch branch = new Branch(Integer.parseInt(parameter[0].trim()), Integer.parseInt(parameter[1].trim()), Integer.parseInt(parameter[2].trim()));
                    list.add(branch);
                    nodes.add(Integer.parseInt(parameter[0].trim()));
                    nodes.add(Integer.parseInt(parameter[1].trim()));
                    edges.add(Integer.parseInt(parameter[2].trim()));
                    
		}
	}
        System.out.print("Network data fetched.\n"
                + "Network contains:\n" + nodes.size() + " nodes and " + edges.size() + " edges.\n");
        return list;
    }
    
}
