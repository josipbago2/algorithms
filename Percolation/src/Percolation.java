/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 *
 * @author Josip
 */
public final class Percolation implements PercolationInterface {

    private int[] grid;
    private int n;
    private int gridSize, upperNodeIndex, lowerNodeIndex;
    
    private WeightedQuickUnionUF qu;
    private int numberOfSitesOpen = 0;

    public Percolation(int N) {
        if(N < 0){
            throw new IllegalArgumentException();
        }
        gridSize = (int) Math.pow(N, 2);
        
        qu = new WeightedQuickUnionUF(gridSize + 2); // +2 for upper and lower node
        upperNodeIndex = gridSize - 1 + 2;
        lowerNodeIndex = gridSize - 1 + 1;
        for(int j = 0; j < N; j++){ // connect upper node with first row and lower node with last row
            qu.union(upperNodeIndex, j);
            qu.union(lowerNodeIndex, gridSize - N + j);
        }
        
        this.n = N;
        grid = new int[gridSize];
    }

    @Override
    public void open(int row, int col) {
        if(!isOpen(row, col)){
            grid[row*n + col] = 1;
            numberOfSitesOpen++;
            System.out.println(row*n + col);
            if(row == 0){
                // first row, don't union with upper element
                if(isOpen(row + 1, col)){
                    System.out.println("Union lower");
                    qu.union(row*n + col, row*n + col + n);
                }
            }else if(row == n-1){
                // last row, don't union with below element
                if(isOpen(row - 1, col)){
                    System.out.println("Union upper");
                    qu.union(row*n + col, row*n + col - n);
                }
            } else {
                // union with upper and lower element
                if(isOpen(row - 1, col)){
                    System.out.println("Union upper");
                    qu.union(row*n + col, row*n + col - n);
                }
                if(isOpen(row + 1, col)){
                    System.out.println("Union lower");
                    qu.union(row*n + col, row*n + col + n);
                }
            }
            if(col == 0){
                // first col, don't union with upper element
                if(isOpen(row, col + 1)){
                    System.out.println("Union right");
                    qu.union(row*n + col, row*n + col + 1);
                }
            }else if(col == n-1){
                // last col, don't union with below element
                if(isOpen(row, col - 1)){
                    System.out.println("Union left");
                    qu.union(row*n + col, row*n + col - 1);
                }
            } else {
                // union with left and right element
                if(isOpen(row, col - 1)){
                    System.out.println("Union left");
                    qu.union(row*n + col, row*n + col - 1);
                }
                if(isOpen(row, col + 1)){
                    System.out.println("Union right");
                    qu.union(row*n + col, row*n + col + 1);
                }
            }
        }
    }

    @Override
    public boolean isOpen(int row, int col) {
        return grid[row*n + col] == 1;
    }

    @Override
    public boolean isFull(int row, int col) {
        return qu.connected(row*n + col, row*n + col - n);
    }

    @Override
    public int numberOfOpenSites() {
        return numberOfSitesOpen;
    }

    @Override
    public boolean percolates() {
        return qu.connected(upperNodeIndex, lowerNodeIndex);
    }
    
}
