/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.Stopwatch;

/**
 *
 * @author Josip
 */
public class PercolationStats implements PercolationStatsInterface {
    
    private int[] results;

    public static void main(String[] args) {
        Stopwatch s = new Stopwatch();
        PercolationStats ps = new PercolationStats(80, 1);
        System.out.println(s.elapsedTime());
                
    }
    
    public PercolationStats(int n, int trials) {
        if(n <= 0 || trials <= 0){
            throw new IllegalArgumentException();
        }
        
        for(int i = 0; i < trials; i++){
            // conduct experiment
            Percolation percolation = new Percolation(n);
            while(!percolation.percolates()){
                int row = StdRandom.uniform(0, n);
                int col = StdRandom.uniform(0, n);
                percolation.open(row, col);
                
            }
            System.out.println(percolation.numberOfOpenSites());
        }
    }

    @Override
    public double mean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double stdev() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double confidenceLo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double confidenceHi() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
