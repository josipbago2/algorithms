/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Josip
 */
public interface PercolationStatsInterface {
    
    public double mean();
    public double stdev();
    public double confidenceLo();
    public double confidenceHi();
    
}
