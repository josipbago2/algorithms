/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Josip
 */
public interface PercolationInterface {
    
    public void open(int row, int col) throws IndexOutOfBoundsException; // open site (row, col) if it is not open already
    public boolean isOpen(int row, int col) throws IndexOutOfBoundsException; // is site (row, col) open?
    public boolean isFull(int row, int col) throws IndexOutOfBoundsException; // is site (row, col) full?
    public int numberOfOpenSites(); // number of open sites
    public boolean percolates(); // does the system percolate?
    
}
