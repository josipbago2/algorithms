/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courseraalgorithms;

import courseraalgorithms.unionfind.UnionFind;

/**
 *
 * @author Josip
 */
public class WeightedQUPathCompression implements UnionFind {
    
    private int[] id;
    private int[] sz;

    public WeightedQUPathCompression(int N) {
        id = new int[N];
        sz = new int[N];
        
        for(int i = 0; i < N; i++){
            id[i] = i;
        }    
    }

    @Override
    public void union(int p, int q) {
        int proot = root(p);
        int qroot = root(q);
        if(proot == qroot){
            return;
        }
        if(sz[proot] < sz[qroot]){
            id[proot] = qroot;
            sz[qroot] += sz[proot];
        }else{
            id[qroot] = proot;
            sz[proot] += sz[qroot];
        }
    }

    @Override
    public boolean connected(int p, int q) {
        return root(p) == root(q);
    }

    @Override
    public int root(int i) {
        while(id[i] != i){
            id[i] = id[id[i]];
            i = id[i];
        }
        return i;
    }
}
