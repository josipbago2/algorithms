/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courseraalgorithms.unionfind;

/**
 *
 * @author Josip
 */
public class QuickUnion implements UnionFind {

    private int[] id;
    
    public QuickUnion(int N) {
        id = new int[N];
        for(int i = 0; i < N; i++){
            id[i] = i; // N array accesses
        }
    }

    // set root of p to be root of q
    @Override
    public void union(int p, int q) {
        int i = root(p);
        int j = root(q);
        
        id[i] = j; // depth of p and q array accesses
    }

    @Override
    public boolean connected(int p, int q) {
        return root(p) == root(q); // depth of p and q array accesses
    }  

    @Override
    public int root(int i) {
        while(i != id[i]){
            i = id[i]; // chase parent pointers, depth of i array accesses
        }
        return i;
    }
    
}
