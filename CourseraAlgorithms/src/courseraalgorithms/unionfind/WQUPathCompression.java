/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courseraalgorithms.unionfind;

/**
 *
 * @author Josip
 */
public class WQUPathCompression extends WeightedQuickUnion{

    private int[] id;
    private int[] sz;
    
    public WQUPathCompression(int N) {
        super(N);
        id = new int[N];
        sz = new int[N];
        for(int i = 0; i < N; i++){
            id[i] = i;
        }
    }

    @Override
    public void union(int p, int q) {
        int i = root(p);
        int j = root(q);
        if(i == j){
            return;
        }
        if(sz[i] < sz[j]){
            id[i] = j;
            sz[j] += sz[i];
        }else{
            id[j] = i;
            sz[i] += sz[j];
        }
    }
    
    

    @Override
    public int root(int i) {
        while(i != id[i]){
            // path compression modification
            id[i] = id[id[i]];
            i = id[i]; // chase parent pointers, depth of i array accesses
        }
        return i;
    }
    
    
    
}
