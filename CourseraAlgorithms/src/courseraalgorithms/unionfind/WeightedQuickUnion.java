/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courseraalgorithms.unionfind;

/**
 *
 * @author Josip
 */
public class WeightedQuickUnion extends QuickUnion {
    
    private int sz[];
    private int[] id;
    
    public WeightedQuickUnion(int N) {
        super(N);
        sz = new int[N];
        id = new int[N];
        for(int i = 0; i < N; i++){
            id[i] = i; // N array accesses
        }
    }

    @Override
    public void union(int p, int q) {
        super.union(p, q);
        int i = root(p);
        int j = root(q);
        if(i == j){
            return;
        }
        if(sz[i] < sz[j]){
            id[i] = j;
            sz[j] += sz[i];
        }else{
            id[j] = i;
            sz[i] += sz[j];
        }
    }
}
