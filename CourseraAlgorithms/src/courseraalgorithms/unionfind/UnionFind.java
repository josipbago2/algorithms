/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courseraalgorithms.unionfind;

/**
 *
 * @author Josip
 */
public interface UnionFind {
    
    public void union(int p, int q);
    
    public boolean connected(int p, int q);
    
    public int root(int i);
}
