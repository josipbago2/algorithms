/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courseraalgorithms.unionfind;

/**
 *
 * @author Josip
 */
public class QuickFind implements UnionFind {

    /**
     * N union commands on N objects = O(N2)
     */
    private int[] id;

    // N
    public QuickFind(int N) {
        id = new int[N];
        for(int i = 0; i < N; i++){ // N array accesses
            id[i] = i;
        }
    }
    
    
    // 2N + 2
    // search for all with same id as id of index p, change id to id of index q
    @Override
    public void union(int p, int q) {
        int pid = id[p]; // array access
        int qid = id[q]; // array access
        
        for(int i = 0; i < id.length; i++){ // N array accesses
            if(id[i] == pid){
                id[i] = qid;
            }
        }
    }

    // 2
    @Override
    public boolean connected(int p, int q) {
        return id[p] == id[q]; // 2 array accesses
    }   

    // not used since all data is stored in array of ids for each node/index
    @Override
    public int root(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
